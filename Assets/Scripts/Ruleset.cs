﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public abstract class Ruleset : MonoBehaviour, IRuleset
	{
		public abstract CellState CheckRules(IEnumerable<CellController> neighbours, CellState currentState);
	}
}
