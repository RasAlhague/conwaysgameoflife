﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public class StandardRuleset : Ruleset
	{
		public int BirthNumber;
		public int DeatchGreaterAs;
		public int DeathLessThan;

		public override CellState CheckRules(IEnumerable<CellController> neighbours, CellState currentState)
		{
			var livingNeighbours = neighbours.Count(x => x.State == CellState.Alive);

			if (currentState == CellState.Dead && livingNeighbours == BirthNumber)
			{
				return CellState.Alive;
			}
			else if (currentState == CellState.Alive && livingNeighbours < DeathLessThan)
			{
				return CellState.HasLived;
			}
			else if (currentState == CellState.Alive && livingNeighbours > DeatchGreaterAs)
			{
				return CellState.HasLived;
			}
			else
			{
				return currentState;
			}
		}
	}
}

