﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public class WorldController : MonoBehaviour
	{
		private CellController[,,] _cells;
		private Ruleset _ruleset;
		[SerializeField]
		private int _roundCount = 0;

		public uint Tables = 20;
		public uint Rows = 20;
		public uint Columns = 20;
		public GameObject CellPrefab;
		public float SpaceMultiplier = 0.2f;
		public float RoundWaitTime = 1f;
		public int RoundsForRandomize = 100;
		public int RandomizeCount = 100;

		// Use this for initialization
		void Start()
		{
			if (Tables == 0)
			{
				Tables = 3;
			}
			if (Rows == 0)
			{
				Rows = 3;
			}
			if (Columns == 0)
			{
				Columns = 3;
			}

			_cells = new CellController[Tables, Rows, Columns];
			_ruleset = gameObject.GetComponent<Ruleset>();

			for (int table = 0; table < Tables; table++)
			{
				for (int row = 0; row < Rows; row++)
				{
					for (int column = 0; column < Columns; column++)
					{
						var temp = Instantiate(CellPrefab, new Vector3(row * SpaceMultiplier, column * SpaceMultiplier, table * SpaceMultiplier), Quaternion.identity);

						_cells[table, row, column] = temp.GetComponent<CellController>();
						_cells[table, row, column].Rules = _ruleset;
						_cells[table, row, column].WorldZPosition = table;
						_cells[table, row, column].WorldXPosition = row;
						_cells[table, row, column].WorldYPosition = column;
					}
				}
			}

			for (int table = 0; table < Tables; table++)
			{
				for (int row = 0; row < Rows; row++)
				{
					for (int column = 0; column < Columns; column++)
					{
						CellController cell = this._cells[table, row, column];

						CheckIndexForNeigbourCol(table - 1, row - 1, column - 1, cell);
						CheckIndexForNeigbourCol(table - 1, row - 1, column, cell);
						CheckIndexForNeigbourCol(table - 1, row - 1, column + 1, cell);
						CheckIndexForNeigbourCol(table - 1, row, column - 1, cell);
						CheckIndexForNeigbourCol(table - 1, row, column, cell);
						CheckIndexForNeigbourCol(table - 1, row, column + 1, cell);
						CheckIndexForNeigbourCol(table - 1, row + 1, column - 1, cell);
						CheckIndexForNeigbourCol(table - 1, row + 1, column, cell);
						CheckIndexForNeigbourCol(table - 1, row + 1, column + 1, cell);

						CheckIndexForNeigbourCol(table, row - 1, column - 1, cell);
						CheckIndexForNeigbourCol(table, row - 1, column, cell);
						CheckIndexForNeigbourCol(table, row - 1, column + 1, cell);
						CheckIndexForNeigbourCol(table, row, column - 1, cell);
						CheckIndexForNeigbourCol(table, row, column + 1, cell);
						CheckIndexForNeigbourCol(table, row + 1, column - 1, cell);
						CheckIndexForNeigbourCol(table, row + 1, column, cell);
						CheckIndexForNeigbourCol(table, row + 1, column + 1, cell);

						CheckIndexForNeigbourCol(table + 1, row - 1, column - 1, cell);
						CheckIndexForNeigbourCol(table + 1, row - 1, column, cell);
						CheckIndexForNeigbourCol(table + 1, row - 1, column + 1, cell);
						CheckIndexForNeigbourCol(table + 1, row, column - 1, cell);
						CheckIndexForNeigbourCol(table + 1, row, column, cell);
						CheckIndexForNeigbourCol(table + 1, row, column + 1, cell);
						CheckIndexForNeigbourCol(table + 1, row + 1, column - 1, cell);
						CheckIndexForNeigbourCol(table + 1, row + 1, column, cell);
						CheckIndexForNeigbourCol(table + 1, row + 1, column + 1, cell);
					}
				}
			}

			InvokeRepeating("NextRound", RoundWaitTime, RoundWaitTime);
		}

		private void CheckIndexForNeigbourCol(int table, int row, int column, CellController cell)
		{
			if (table >= 0 && table < Tables && row >= 0 && row < Rows && column >= 0 && column < Columns)
			{
				cell.Neighbours.Add(_cells[table, row, column]);
			}
		}

		public void NextRound()
		{
			foreach (var cell in _cells)
			{
				cell.SetTempState();
			}

			foreach (var cell in _cells)
			{
				cell.SetNewState();
			}
			_roundCount++;
		}

		public void RandomizeField(int count)
		{
			for (int i = 0; i < count; i++)
			{
				System.Random random = new System.Random();

				int table = random.Next(0, Convert.ToInt32(Tables - 1));
				int row = random.Next(0, Convert.ToInt32(Rows - 1));
				int column = random.Next(0, Convert.ToInt32(Columns - 1));

				Thread.Sleep((int)(Time.deltaTime * 1000));

				_cells[table, row, column].State = CellState.Alive;
			}
		}
	}
}