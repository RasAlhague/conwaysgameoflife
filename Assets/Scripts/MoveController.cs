﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public class MoveController : MonoBehaviour
	{
		//Bewegungsrichtung
		public float moveSpeed = 5.0F;
		//Drehgeschwindigkeit
		public float rotationSpeed = 300.0F;
		public float rotateMeter = 2.0f;
		//Bewegungsrichtung
		private Vector3 moveDirection = Vector3.zero;
		//CharacterController-Speichervariable
		private CharacterController controller;
		//Zieldrehungsausrichtung
		private Quaternion destRotation;
		void Start()
		{
			controller = GetComponent<CharacterController>();
			//Initialisierung: Anfangsausrichtung als Zieldrehung
			destRotation = transform.rotation;
		}
		void Update()
		{
			//Bewegungstasten abfragen und der Variablen zuweisen
			moveDirection = new Vector3(Input.GetAxis("Horizontal"),
			0, Input.GetAxis("Vertical"));
			//Umrechnung der relativen Richtung in globale Richtung
			moveDirection = transform.TransformDirection(moveDirection);
			// Geschwindigkeitsfaktor einbeziehen
			moveDirection = moveDirection * moveSpeed;
			//Controller mit SimpleMove steuern
			controller.Move(moveDirection * Time.deltaTime);
			//Wenn Taste "Left" gedrueckt wird
			if (Input.GetButton("Left"))
			{
				//Dann Zieldrehung 90 Grad nach links
				destRotation.eulerAngles = destRotation.eulerAngles - new Vector3(0, rotateMeter, 0);
			}
			//Wenn Taste "Right" gedrueckt wird
			if (Input.GetButton("Right"))
			{
				//Dann Zieldrehung 90 Grad nach rechts
				destRotation.eulerAngles = destRotation.eulerAngles + new Vector3(0, rotateMeter, 0);
			}
			if (Input.GetButton("Up"))
			{
				//Dann Zieldrehung 90 Grad nach links
				destRotation.eulerAngles = destRotation.eulerAngles - new Vector3(rotateMeter, 0, 0);
			}
			//Wenn Taste "Right" gedrueckt wird
			if (Input.GetButton("Down"))
			{
				//Dann Zieldrehung 90 Grad nach rechts
				destRotation.eulerAngles = destRotation.eulerAngles + new Vector3(rotateMeter, 0, 0);
			}
			//Schrittweite fuer den aktuellen Rotationsschritt berechnen
			float step = rotationSpeed * Time.deltaTime;
			//Transform drehen
			transform.rotation = Quaternion.RotateTowards(transform.rotation, destRotation, step);
		}
	}
}