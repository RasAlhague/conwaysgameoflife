﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public enum CellState
	{
		Dead,
		Alive,
		HasLived
	}
}