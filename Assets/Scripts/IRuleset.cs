﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public interface IRuleset
	{
		CellState CheckRules(IEnumerable<CellController> neighbours, CellState currentState);
	}
}