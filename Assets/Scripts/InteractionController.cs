﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public class InteractionController : MonoBehaviour
	{
		private WorldController worldController;

		void Awake()
		{
			worldController = GameObject.FindObjectOfType<WorldController>(); 
		}

		// Update is called once per frame
		void Update()
		{
			if(Input.GetButtonDown("Fire1"))
			{
				worldController.CancelInvoke("NextRound");
				worldController.RandomizeField(worldController.RandomizeCount);

				worldController.InvokeRepeating("NextRound", worldController.RoundWaitTime, worldController.RoundWaitTime);
			}
		}
	}
}

