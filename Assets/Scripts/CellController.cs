﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConwaysGameOfLife
{
	public class CellController : MonoBehaviour
	{
		private CellState _tempState;
		private Renderer _rend;

		public Ruleset Rules;
		public int WorldXPosition;
		public int WorldYPosition;
		public int WorldZPosition;
		public CellState State;
		public List<CellController> Neighbours = new List<CellController>();

		// Use this for initialization
		void Start()
		{
			_rend = GetComponent<Renderer>();
			State = CellState.Dead;
			_tempState = State;
		}

		public virtual void SetTempState()
		{
			_tempState = Rules.CheckRules(Neighbours, this.State);
		}

		public virtual void SetNewState()
		{
			if(_tempState != State)
			{
				State = _tempState;
				_rend.enabled = State == CellState.Alive;
			}
		}
	}
}
